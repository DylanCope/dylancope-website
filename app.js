var isOpen = false;
var currentPageIndex = 0;
var selectedMenuItem = $('.home_button');
var menu  = [$('.home_button'), $('.about_button'), $('.projects_button'), $('.edu_button'), $('.contact_button') ];
var pages = [$('.home_div'), $('.about_div'), $('.projects_div'), $('.edu_div'), $('.contact_div')];

var openMenu = function()
{
  $('.menu').animate({
    left: "0px"
  }, 200);
  
  $('.overlay').show();

  $('.overlay').animate({
    opacity: "0.8"
  }, 150);

  isOpen = true;
}

var closeMenu = function()
{
  $('.menu').animate({
    left: "-285px"
  }, 200);

  $('.overlay').animate({
    opacity: "0"
  }, 150);

  setTimeout(function() { $('.overlay').hide(); }, 200);

  isOpen = false;
}

var toggleMenu = function()
{
  if (isOpen)
    closeMenu();
  else
    openMenu();
}

var initAnimate = function() 
{
  $('.dylan_text').animate({
    opacity: 1,
    left: 100
  }, 1500);

  setTimeout(function() {
    $('.cope_text').animate({
      opacity: 1,
      left: 300
    }, 1500);
  }, 200);
}

var setMenuItemSelected = function(dest)
{
  var menuItem = menu[dest];
  currentPageIndex = dest;
  selectedMenuItem.animate({'background-color': 'rgba(1, 1, 1, 0)'}, 100);
  menuItem.animate({'background-color': '#DDD'}, 100);
  selectedMenuItem = menuItem;
}

var updateArrows = function()
{
  if (currentPageIndex === 0) {
    $('.left_arrow').animate({
      opacity: 0,
      left: "-10%" 
    }, 400, "swing");
  }
  else {
    $('.left_arrow').animate({ 
      opacity: 1,
      left: "0%" 
    }, 400, "swing");
  }

  if (currentPageIndex === pages.length - 1) {
    $('.right_arrow').animate({
      opacity: 0, 
      left: "100%" 
    }, 400, "swing");
  }
  else {
    $('.right_arrow').animate({
      opacity: 1, 
      left: "90%" 
    }, 400, "swing");
  }
}

var smoothScrollUp = function()
{
  var time = 500;
  var start = window.pageYOffset;
  var step = time / 30;
  var t = 0;
  var k =  - start / 2;

  var recurScroll = function()
  {
    if (t >= time)
      window.scroll(0, 0);
    else 
    {
      var x = start + k * (1 - Math.cos(Math.PI * t / time));
      t += step;
      window.scroll(0, x);
      setTimeout(recurScroll, step);
    }

  }

  recurScroll();
}

var goToPage = function(dest)
{
  var time = Math.abs(500 * (dest - currentPageIndex));
  if (dest >= 0 && dest < pages.length)
  {
    pages.forEach(function(page) 
    {
      var i = pages.indexOf(page);
      page.animate({ 
        opacity: dest - (i-1),
        left: $(window).width() * (i - dest)
      }, time, "swing");
    });
    setMenuItemSelected(dest);
    updateArrows();
    smoothScrollUp();
  }
}

var isSelectedMenuItem = function(menuItem)
{
  return menu.indexOf(menuItem) == menu.indexOf(selectedMenuItem);
}

var scrollEventHandler = function()
{
  window.scroll(0, window.pageYOffset)
}

var main = function() 
{
  initAnimate();
  $('.overlay').hide();
  $('.overlay').click(toggleMenu);
  $('.icon-menu').click(toggleMenu);

  $('.content').scroll(function() {
    console.log(document.scrollTop());
  });

  menu.forEach(function(item) 
  {
    var onClick = function() { 
      closeMenu();  
      goToPage(menu.indexOf(item));
    };

    var mouseEnter = function() {
      if (!isSelectedMenuItem(item))
        item.css('background-color', '#EEE');
    };

    var mouseLeave = function() {
      if (!isSelectedMenuItem(item))
        item.css('background-color', 'rgba(1, 1, 1, 0)');
    };

    item.click(onClick);
    item.mouseenter(mouseEnter);
    item.mouseleave(mouseLeave);
  });

  $('.right_arrow').click(function()
  {
    goToPage(currentPageIndex + 1)
  });

  $('.left_arrow').click(function()
  {
    goToPage(currentPageIndex - 1)
  });
};

$(document).ready(main);
window.addEventListener("scroll", scrollEventHandler, false);
$(window).resize(function() {
  goToPage(currentPageIndex);
});